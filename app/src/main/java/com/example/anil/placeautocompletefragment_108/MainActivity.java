package com.example.anil.placeautocompletefragment_108;

//working properly  you may get  places api not configured error just enable api from developer console

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PlaceAutocompleteFragment places= (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        //adding filters
        places.setFilter((new AutocompleteFilter.Builder().setCountry("in")).build());
        places.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                Toast.makeText(getApplicationContext(),place.getName(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Status status) {

                Toast.makeText(getApplicationContext(),status.toString(),Toast.LENGTH_SHORT).show();

            }
        });
    }

    /*some more filters
    TYPE_FILTER_NONE
TYPE_FILTER_GEOCODE
TYPE_FILTER_ADDRESS
TYPE_FILTER_ESTABLISHMENT
TYPE_FILTER_REGIONS
TYPE_FILTER_CITIES


AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
        .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
        .build();

places.setFilter(typeFilter);

http://codesfor.in/android-places-autocomplete-example/
    */
}
